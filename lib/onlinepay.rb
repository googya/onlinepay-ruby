require 'cgi'
require 'set'
require 'openssl'
require 'rest-client'
require 'json'
require 'base64'
require 'socket'
require 'rbconfig'

require 'pry'

require 'onlinepay/api_operations/request'
require 'onlinepay/api_operations/create'
require 'onlinepay/api_operations/list'

require "onlinepay/version"
require 'onlinepay/util'
require 'onlinepay/onlinepay_object'
require 'onlinepay/api_resource'
require 'onlinepay/payment'
require 'onlinepay/list_object'
require 'onlinepay/singleton_api_resource'

require 'onlinepay/errors/onlinepay_error'
require 'onlinepay/errors/authentication_error'

module Onlinepay
  DEFAULT_CA_BUNDLE_PATH = File.dirname(__FILE__) + '/data/ca-certificates.crt'

  # TODO: should be changed
  @api_base = 'https://business.sandbox.onlinepay.com'


  @ca_bundle_path = DEFAULT_CA_BUNDLE_PATH

  @verify_ssl_certs = false

  HEADERS_TO_PARSE = [:onlinepay_one_version, :onlinepay_sdk_version]

  class << self
    attr_accessor :api_key, :api_base, :verify_ssl_certs, :api_version,
                  :parsed_headers, :private_key
  end

  def self.api_url(url = '', api_base_url = nil)
    (api_base_url || @api_base) + url
  end

  def self.parse_headers(headers)
    @parsed_headers = {}
    if headers && headers.respond_to?("each")
      headers.each do |k, v|
        k = k[0, 5] == 'HTTP_' ? k[5..-1] : k
        header_key = k.gsub(/-/, '_').to_s.downcase.to_sym
        if HEADERS_TO_PARSE.include?(header_key)
          if v.is_a?(String)
            @parsed_headers[header_key] = v
          elsif v.is_a?(Array)
            @parsed_headers[header_key] = v[0]
          end
        end
      end
    end
  end


  def self.request(method, url, api_key, params = {}, headers = {}, api_base_url = nil)
    api_base_url ||= @api_base

    unless api_key ||= @api_key
      raise AuthenticationError.new('No private_key ')
    end

    if api_key =~ /\s/
      raise AuthenticationError.new('invalid private_key')
    end

    request_opts = { verify_ssl: false, ssl_version: 'TLSv1' }

    # binding.pry

    params = Util.objects_to_ids(params)
    url = api_url(url, api_base_url)
    method_sym = method.to_s.downcase.to_sym



    case method_sym

    when :get, :head, :delete
      url += "#{URI.parse(url).query ? '&' : '?'}#{Util.encode_parameters(params)}" if params && params.any?
      payload = nil
    else
      payload = JSON.generate(params)
    end



    request_opts.update(
      headers: request_headers(api_key, method_sym, payload, url).update(headers),
      method: method,
      payload: payload,
      url: url
    )

    response = execute_request_with_rescues(request_opts, api_base_url)

    parse(response)
  end

  private 
  def self.user_agent
    @uname ||= get_uname
    lang_version = "#{RUBY_VERSION} p#{RUBY_PATCHLEVEL} (#{RUBY_RELEASE_DATE})"

    {
      :bindings_version => Onlinepay::VERSION,
      :lang => 'ruby',
      :lang_version => lang_version,
      :platform => RUBY_PLATFORM,
      :publisher => 'onlinepay',
      :uname => @uname,
      :hostname => Socket.gethostname,
      :engine => defined?(RUBY_ENGINE) ? RUBY_ENGINE : '',
      :openssl_version => OpenSSL::OPENSSL_VERSION
    }
  end

   def self.get_uname
    if File.exist?('/proc/version')
      File.read('/proc/version').strip
    else
      case RbConfig::CONFIG['host_os']
      when /linux|darwin|bsd|sunos|solaris|cygwin/i
        _uname_uname
      when /mswin|mingw/i
        _uname_ver
      else
        "unknown platform"
      end
    end
  end

  def self._uname_uname
    (`uname -a 2>/dev/null` || '').strip
  rescue Errno::ENOMEM
    "uname lookup failed"
  end

  def self._uname_ver
    (`ver` || '').strip
  rescue Errno::ENOMEM
    "uname lookup failed"
  end

  def self.execute_request_with_rescues(request_opts, api_base_url, retry_count = 0)
    begin
      response = execute_request(request_opts)
    rescue => e
      raise
    end

    response
  end


  def self.request_headers(api_key, method_sym, data, url)
    post_or_put = (method_sym == :post or method_sym == :put)
    headers = {
      :user_agent => "Onlinepay/v1 RubyBindings/#{Onlinepay::VERSION}",
      :authorization => "Bearer #{api_key}",
      :content_type => post_or_put ? 'application/json' : 'application/x-www-form-urlencoded'
    }

    headers[:onlinepay_version] = api_version if api_version
    headers.update(parsed_headers) if parsed_headers && !parsed_headers.empty?

    begin
      headers.update(:x_onlinepay_client_user_agent => JSON.generate(user_agent))
    rescue => e
      headers.update(:x_onlinepay_client_raw_user_agent => user_agent.inspect,
                     :error => "#{e} (#{e.class})")
    end

    data_to_be_signed = data || ''
    uri = URI.parse(url)
    data_to_be_signed += uri.path
    (!uri.query.nil?) && data_to_be_signed += '?' + uri.query

    request_time = Time.now.to_i.to_s
    headers.update(:onlinepay_request_timestamp => request_time)
    data_to_be_signed += request_time

    headers
  end

  def self.execute_request(opts)
    puts opts.inspect
    RestClient::Request.execute(opts)
  end

  def self.parse(response)
    begin
      response = JSON.parse(response.body)
    rescue JSON::ParserError
      raise general_api_error(response.code, response.body)
    end

    Util.symbolize_names(response)
  end
end
