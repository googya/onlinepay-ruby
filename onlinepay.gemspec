# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'onlinepay/version'

Gem::Specification.new do |spec|
  spec.name          = "onlinepay"
  spec.version       = Onlinepay::VERSION
  spec.authors       = ["leslie.wen"]
  spec.email         = ["wen.xs@ikcrm.com"]

  spec.summary       = "Ruby bindings for the Onlinepay API"
  spec.description   = "Onlinepay Transfer money from one user to another socially, split the bill in a fun way.  See https://onlinepay.com/ for details"
  spec.homepage      = "https://bitbucket.org/googya/onlinepay-ruby"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency('rest-client', '>= 1.4', '< 4.0')

  spec.add_development_dependency "bundler", "~> 1.12"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
end
